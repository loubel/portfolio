$(document).ready(function () {

    $("#r1").click(function () {
        $(".fenCode2").fadeOut(10);
        $(".fenCode2").queue(function () {
            setTimeout(function () {
                $(".fenCode2").dequeue();
            }, 2000);
        });
        $(".fenCode2").fadeIn(10);
    });
    $("#menuburger").click(function (e) {
        e.stopPropagation();
        $("#menuburger").toggleClass('open');
        if ($("nav").hasClass("navReduit")) {
            $("nav").removeClass("navReduit");
        } else {
            $("nav").addClass("navReduit");
        }
    });
    $(document).click(function () {
        if ($("nav").hasClass("navReduit")) {
            $("nav").removeClass("navReduit");
            $("#menuburger").removeClass('open');
        }
    });
 
    $(window).bind("resize", function () {
        if ($(this).width() > 1500) {
            $("nav").removeClass("navReduit");
        }
    }).trigger('resize');

});

window.customElements.define('bar-chargement', class extends HTMLElement {
    connectedCallback() {
        this.innerHTML = `
            <div id="preloader" class="d-flex flex-column justify-content-center">
                <p> Démarrage ...</p>
                <div class="progress">
                    <div class="progress-value"></div>
                </div>
            </div>`;
    }
});

window.customElements.define('menu-burger', class extends HTMLElement {
    connectedCallback() {
        this.innerHTML = `
        <div id="divBurger">
            <div id="menuburger">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>`;
    }
});

window.customElements.define('nav-c', class extends HTMLElement {
    connectedCallback() {
        this.innerHTML = `
    <nav id="nav" class="flex-row justify-content-center listeBtn2">
        <button id="b0" class="btn-success" onclick="window.location='index.html';">getProfil()</button>
        <button id="b1" class="btn-success" onclick="window.location='experiences.html';">getExpériences()</button>
        <button id="b2" class="btn-success" onclick="window.location='competences.html';">getCompétences()</button>
        <button id="b3" class="btn-success" onclick="window.location='formations.html';">getFormations()</button>
        <button id="b4" class="btn-success" onclick="window.location='projets.html';">getProjets()</button>
        <button id="b5" class="btn-success" onclick="window.location='contact.php';">getContact()</button>
        <button id="b6" class="btn-success" onclick="window.open('doc/cv-louan-belicaud-2021.pdf')">getCV()</button>
    </nav>
    `;
    }
});