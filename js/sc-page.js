$(document).ready(function() {

    // Style écriture de texte API Typewriter
    var typewriter = new Typewriter(app, {
        loop: false,
        delay: 50,
    });

    typewriter
        .pauseFor(750)
        .typeString('class <span class="jaune">Louan</span> <span class="bleu">{</span><br /><span class="ligne">String <span class="gris">nom</span> = <span class="bleu">"</span><span class="vert">Louan Bélicaud</span><span class="bleu">";</span></span><br /><span class="ligne">int <span class="gris">âge</span> = <span class="rouge">18</span><span class="bleu">;</span></span><br /><span class="ligne bool">boolean <span class="gris">estVéhiculé</span> = <span class="rouge">true</span><span class="bleu">;</span></span><br /><span class="ligne outilsdiv"><span class="outils">String<span class="bleu">[]</span> <span class="gris">autresLangues</span> = </span><div class="te" onclick="ouvrirPM(this)"><img width="50px" src="image/anglais.png" alt="anglais" title="anglais"><p>Avancé</p></div><div class="te" onclick="ouvrirPM(this)"><img width="50px" src="image/espagnol.png" alt="Espagnol" title="Espagnol"><p>Notions</p></div><span class="bleu">;</span></span><br /><span class="ligne">String<span class="bleu">[]</span> <span class="gris">passions</span> = <span class="bleu">{"</span><span class="vert">Voile</span><span class="bleu">", "</span><span class="vert">Entrepreneuriat</span><span class="bleu">", "</span><span class="vert">Nouvelles technologies</span><span class="bleu">", "</span><span class="vert">Exploration spatiale</span><span class="bleu">"};</span><br/><span class="bleu">}</span>').start();

    $("#r1").click(function() {
        $(".fenCode").fadeOut(10);
        $(".fenCode").queue(function() {
            setTimeout(function() {
                $(".fenCode").dequeue();
            }, 2000);
        });
        $(".fenCode").fadeIn(10);
    });


    $("#nav").hide();

    setTimeout(
        function() {
            $("#nav").fadeIn(700);
        }, 2000);




    dragElement(document.getElementById("movediv"));

    function dragElement(elmnt) {
        var pos1 = 0,
            pos2 = 0,
            pos3 = 0,
            pos4 = 0;

        document.getElementById("bord").onmousedown = dragMouseDown;

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            console.log(window.innerWidth - elmnt.offsetLeft - elmnt.offsetWidth * 0.5);
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        }

        function closeDragElement() {
            // stop moving when mouse button is released:
            document.onmouseup = null;
            document.onmousemove = null;
            if ((elmnt.offsetLeft - elmnt.scrollLeft - elmnt.offsetWidth * 0.5 < 0 /* gauche */ ) || (elmnt.offsetTop - elmnt.scrollTop - elmnt.offsetHeight * 0.5 < 0 /* haut */ ) ||
                (window.innerWidth - elmnt.offsetLeft - elmnt.offsetWidth * 0.5 < 0 /* droite */ ) || (window.innerHeight - elmnt.offsetTop - elmnt.offsetHeight * 0.5 < 0 /* bas */ )) {
                //si dépasse à gauche
                elmnt.style.top = 50 + "%";
                elmnt.style.left = 50 + "%";

            }
        }
    }





});