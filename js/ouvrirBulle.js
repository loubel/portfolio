function ouvrirPM(div) {
    if ($(div).hasClass('pm')) {
        $(div).removeClass('pm');
    } else {
        $(div).addClass('pm');
        $(div).delay(5000).queue(function () {
            $(div).removeClass('pm');
            $(div).dequeue();
        });
    }
}
function ouvrirPT(div) {
    if ($(div).hasClass('pt')) {
        $(div).removeClass('pt');
    } else {
        $(div).addClass('pt');
        $(div).delay(5000).queue(function () {
            $(div).removeClass('pt');
            $(div).dequeue();
        });
    }
}
function ouvrirPG(div) {
    if ($(div).hasClass('pg')) {
        $(div).removeClass('pg');
    } else {
        $(div).addClass('pg');
        $(div).delay(5000).queue(function () {
            $(div).removeClass('pg');
            $(div).dequeue();
        });
    }
}
function ouvrir(div) {
    if ($(div).hasClass('ps')) {
        $(div).removeClass('ps');
    } else {
        $(div).addClass('ps');
        $(div).delay(5000).queue(function () {
            $(div).removeClass('ps');
            $(div).dequeue();
        });
    }
}
function ouvrirPH(div) {
    if ($(div).hasClass('ph')) {
        $(div).removeClass('ph');
    } else {
        $(div).addClass('ph');
        $(div).delay(5000).queue(function () {
            $(div).removeClass('ph');
            $(div).dequeue();
        });
    }
}
