$(document).ready(function () {
    $('#divPhoto').hide();
    $("#btnAff3").click(function () {
        if ($(this).text() === "Afficher") {
            $("#divPhoto").fadeIn(1500);
            $('#divPhoto').attr('style', 'display: flex');
            $("#btnAff3").text("Fermer");
        } else {
            $("#divPhoto").fadeOut(1500);
            $("#btnAff3").text("Afficher");
        }
    });
});
