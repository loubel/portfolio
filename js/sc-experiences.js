$(document).ready(function () {
    $("#carouselItems").hide();
    $("#btnAff").click(function () {
        if ($(this).text() === "Afficher") {
            $("#carouselItems").fadeIn(1500);
            $("#btnAff").text("Fermer");
        } else {
            $("#carouselItems").fadeOut(1500);
            $("#btnAff").text("Afficher");
        }
    });
});
