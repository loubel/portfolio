$(document).ready(function () {
    $('#divYtb').hide();
    $("#btnAff2").click(function () {
        if ($(this).text() === "Afficher") {
            $("#divYtb").fadeIn(1500);
            $('#divYtb').attr('style', 'display: flex !important');
            $("#btnAff2").text("Fermer");
        } else {
            $("#divYtb").fadeOut(1500);
            $("#btnAff2").text("Afficher");
        }
    });
});
